from sqlalchemy import *
from sqlalchemy.orm import *
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
engine = create_engine('sqlite:///./db.db', echo=True)
 

class Worktime(Base):
    
    ##table name
    __tablename__ = 'worktimes'
    ##カラム名
    id = Column(Integer, primary_key = True)
    name =  Column(Text)
    day = Column(Text)
    attend = Column(Text)
    leave = Column(Text)


def create_tables():
    Base.metadata.create_all(engine)


if __name__ == "__main__":
    create_tables()
