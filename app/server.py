from flask import Flask

app = Flask(__name__)


@app.route("/")
def top():
    return "this is top page"


if __name__ == "__main__":
    app.run()

